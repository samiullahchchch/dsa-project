import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
import pandas as pd
from time import sleep
from bs4 import BeautifulSoup
import numpy as np
from selenium.webdriver.chrome.service import Service



service = Service(executable_path='A:\chromedriver-win64/chromedriver.exe')
option=webdriver.ChromeOptions()
option.add_argument("--headless=new")#making it headless
option.add_argument('--no-sandbox')
option.add_argument('--disable-dev-shm-usage')
option.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36')
driver = webdriver.Chrome(service=service)


#--------------------------------arrays for storage scrapped data
name_of_toy = []
Condition = []
Price_list = []
Shipping_list_price = []
feedback_list = []
returns_time_list = []
Shipping_city_list = []
Seller_info_list = []
Rating_list = []
rating = 0
feed_back = 0
valid = []


#------------------------------------------------valid proxy to change the ip address
with open('Working_Proxy.txt','r') as f:
   valid = f.read().split('\n')




#https://www.ebay.com.sg/sch/i.html?_from=R40&_nkw=toy&_sacat=220&_ipg=240&_dmd=1&rt=nc&_pgn=
#-------------------------------------------------loop to acess and tranverse the page
for page in range(1,32):
   print(page)
   res=requests.get("https://www.ebay.com.sg/sch/i.html?_from=R40&_nkw=Hot+Wheels+cars&_sacat=0&LH_TitleDesc=0&_dmd=1&_ipg=240&_pgn="+ str(page),proxies={'http': valid[page % len(valid)]})
   sleep(15)
   content = res.text
   soup = BeautifulSoup(content,'html.parser')

#------------------------------------------------loop to find the the attribute of the item

   for a in soup.findAll('div', attrs={'class', 's-item__wrapper clearfix'}):
      name = (a.find('div', attrs={'class': 's-item__title'})).find('span')
      status = a.find('span', attrs={'class': 'SECONDARY_INFO'})
      price = a.find('span', attrs={'class': 's-item__price'})
      available = a.find('span', attrs={'class': 's-item__dynamic s-item__purchaseOptionsWithIcon'})
      shippingPrice = a.find('span', attrs={'class': 's-item__shipping s-item__logisticsCost'})
      return_condtions = a.find('span', attrs={'class': 's-item__free-returns s-item__freeReturnsNoFee'})
      shippingCity = a.find('span', attrs={'class': 's-item__location s-item__itemLocation'})
      viewer = a.find('span', attrs={'class': 'BOLD'})
      seller_info = a.find('span', attrs={'class': 's-item__seller-info'})

#-----------------------------------------------we split the info into three parts
      if seller_info:

       split_2 = seller_info.text.split(' ')
       if len(split_2) >= 3:
          seller_first_info = split_2[0]
          seller_rating = split_2[1]
          feed_back = split_2[2]

#------------------------------------------------------------  remove the parentheses
          seller_rating = seller_rating.strip("()")
          split_3 = seller_rating.split(',')
          if len(split_3) == 2:
            rating = split_3[1]
          if len(split_3) == 1:
             rating = split_3[0]




      if name:
         name_of_toy.append(name.text)
      else:
         name_of_toy.append('None')
      if status:
         Condition.append(status.text)
      else:
         Condition.append('None')
      if price:
         Price_list.append(price.text)
      else:
         Price_list.append('None')

      if shippingPrice:
         Shipping_list_price.append(shippingPrice.text)
      else:
         Shipping_list_price.append('None')
      if viewer:
         feedback_list.append(feed_back)
      else:
         feedback_list.append('0')
      if return_condtions:
         returns_time_list.append(return_condtions.text)
      else:
         returns_time_list.append('No Return')
      if shippingCity:

         split_1 = shippingCity.text.split(' ')
         if len(split_1) >= 1:
          shippingCity_name = split_1[1]

         Shipping_city_list.append(shippingCity_name)
      else:
         Shipping_city_list.append('None')
      if seller_info:
         Seller_info_list.append(seller_first_info)

      else:
         Seller_info_list.append('No Information')
      if rating:
         Rating_list.append(rating)
      else:
         Rating_list.append('0')


#-----------------------------------------------at the end of the program we store the data from arrays to csv file
df = pd.DataFrame(
         {'Product Name': name_of_toy, 'Price': Price_list, 'Condition': Condition,'Shipping Price': Shipping_list_price,
          'Shipping City': Shipping_city_list,  'Seller name': Seller_info_list ,'Feedback': feedback_list, 'Rating ' : Rating_list})
df.to_csv('A:\scrapping data\\222.csv',  mode='a', index=False, encoding='utf-8')




