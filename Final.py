import sys
import time
import csv
from PyQt5 import QtWidgets, QtGui


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()


        #starting view of frame
        self.setWindowTitle("CSV Data Viewer")
        self.setGeometry(100, 100, 600, 400)





        self.centralWidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralWidget)
        self.layout = QtWidgets.QHBoxLayout(self.centralWidget)

        # create  container for  sorting buttons
        self.buttonContainer = QtWidgets.QWidget(self.centralWidget)
        self.layout.addWidget(self.buttonContainer)

        self.buttonLayout = QtWidgets.QVBoxLayout(self.buttonContainer)







        button_names = ["Merge Sort", "Insertion Sort", "Bubble Sort", "Selection Sort",
                        "Hybrid Merge Sort", "Counting Sort", "Quick Sort", "Radix Sort",
                        "Bucket Sort", "Shell Sort", "Gnome Sort", "Comb Sort"]


        #check the buttons which is on
        for name in button_names:
            button = QtWidgets.QPushButton(name)
            button.clicked.connect(self.handle_button_click)
            self.buttonLayout.addWidget(button)
            if name == "Bubble Sort":
                self.bubbleSortButton = button
            if name == "Merge Sort":
                self.merge_sort_button = button
            if name == "Insertion Sort":
                self.insertion_sort_button = button
            if name == "Selection Sort":
                self.selection_sort_button = button
            if name == "Hybrid Merge Sort":
                self.hybrid_merge_sort_button = button
            if name == "Counting Sort":
                self.counting_sort_button = button
            if name == "Quick Sort":
                self.quick_sort_button = button
            if name == "Radix Sort":
                self.radix_sort_button = button
            if name == "Bucket Sort":
                self.bucket_sort_button = button
            if name == "Shell Sort":
                self.shell_sort_button = button
            if name == "Gnome Sort":
                self.gnome_sort_button = button
            if name == "Comb Sort":
                self.combo_sort_button = button

            button.setStyleSheet("background-color:  #00FFFF; color: black;")

        # Create a container for widgets on the right
        self.widgetContainer = QtWidgets.QWidget(self.centralWidget)
        self.layout.addWidget(self.widgetContainer)
        self.rightLayout = QtWidgets.QVBoxLayout(self.widgetContainer)

        self.time_label = QtWidgets.QLabel("Sorting Time: 0.0 seconds")
        self.rightLayout.addWidget(self.time_label)

        #create the button for linear search
        linear_button = QtWidgets.QPushButton("Linear S")
        linear_button.setFixedSize(100, 30)
        self.rightLayout.addWidget(linear_button)

        #and connect with linear search function
        linear_button.clicked.connect(self.linear_search_button_clicked)



        # create the text box for linear search
        self.linersearch_box = QtWidgets.QLineEdit()
        self.linersearch_box.setMaximumSize(250, 20)
        self.rightLayout.addWidget(self.linersearch_box)


        #----------------------------create the radio button  and on in starting positon
        self.single_button = QtWidgets.QRadioButton("Single level")
        self.single_button.setChecked(True)
        self.single_button.setObjectName("single_button")
        self.rightLayout.addWidget(self.single_button)

        # ----------------------------create the radio button for multi level and on in starting position
        self.multi_button = QtWidgets.QRadioButton("Multi Level")
        self.multi_button.setChecked(True)
        self.multi_button.setObjectName("multi_button")
        self.rightLayout.addWidget(self.multi_button)




        #create  and fixed the value of progress bar
        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.setValue(24)
        self.rightLayout.addWidget(self.progressBar)


        # create a QTableWidget to display the data of our csv file and make the eight columns and call the function of csv file data
        self.tableWidget = QtWidgets.QTableWidget()
        self.tableWidget.setColumnCount(8)
        self.tableWidget.setHorizontalHeaderLabels(
            ["Column 0", "Column 1", "Column 2", "Column 3", "Column 4", "Column 5", "Column 6", "Column 7"])
        self.rightLayout.addWidget(self.tableWidget)

        self.load_data_from_csv()



        # create a label and a line edit for the column index input
        self.label1 = QtWidgets.QLabel("Enter Column Index:")
        self.rightLayout.addWidget(self.label1)



        #ceate the  text box to take column index
        self.columnLineEdit = QtWidgets.QLineEdit()
        self.columnLineEdit.setMaximumSize(100, 20)
        self.rightLayout.addWidget(self.columnLineEdit)


        #in this part we check our radio button which is on and which is off then implement the functions

        if self.multi_button.isChecked():
            self.bubbleSortButton.clicked.connect(self.bubble_sort_button_click_multi)
            self.insertion_sort_button.clicked.connect(self.insertion_sort_button_click_multi)
            self.selection_sort_button.clicked.connect(self.selection_sort_button_click_multi)
            self.merge_sort_button.clicked.connect(self.merge_sort_button_click_multi)
        if self.single_button.isChecked():
            self.bubbleSortButton.clicked.connect(self.bubble_sort_button_click)
            self.insertion_sort_button.clicked.connect(self.insertion_sort_button_click)
            self.selection_sort_button.clicked.connect(self.selection_sort_button_click)
            self.merge_sort_button.clicked.connect(self.merge_sort_button_click)

        #here we check that one radio button is on and then other will be off
        if self.single_button == False:
             self.single_button.setChecked(True)
        if self.multi_button == False:
             self.multi_button.setChecked(True)

    def handle_button_click(self):
        pass  # You can implement this method as needed



  #----------------------buttons-----------------

    # -
    # ----------------Single Column------------------------

    def linear_search_button_clicked(self):
        search_element = self.linersearch_box.text()

        column_index = self.columnLineEdit.text()
        #print(column_index, search_element)
        try:
            column_index = int(column_index)
        except ValueError:
            print("----------------Invalid--------------------")
            return
        self.linear_search(search_element,column_index)


    def bubble_sort_button_click(self):
        column_index_text = self.columnLineEdit.text()
        try:
            column_index = int(column_index_text)
        except ValueError:
            print("----------------Invalid--------------------")
            return

        self.bubble_sort_code(column_index)


    def insertion_sort_button_click(self):
        column_index_text = self.columnLineEdit.text()
        try:
            column_index = int(column_index_text)
        except ValueError:
            print("----------------Invalid--------------------")
            return

        self.insertion_sort_code(column_index)

    def selection_sort_button_click(self):
        column_index_text = self.columnLineEdit.text()
        try:
            column_index = int(column_index_text)
        except ValueError:
            print("----------------Invalid--------------------")
            return
        self.selection_sort_code(column_index)

    def merge_sort_button_click(self):
        column_index_text = self.columnLineEdit.text()
        try:
            column_index = int(column_index_text)
        except ValueError:
            print("----------------Invalid--------------------")
            return
        self.merge_sort_code(column_index)

#---------------------------------------Multi Column-----------------
    def bubble_sort_button_click_multi(self):
        self.bubble_sort_code(0)

    def insertion_sort_button_click_multi(self):
        self.insertion_sort_code(0)

    def selection_sort_button_click_multi(self):
        self.selection_sort_code(0)

    def merge_sort_button_click_multi(self):
        self.merge_sort_code(0)



    # ------------------codes-------------------------


    def merge(self,left, right, column_index):
        result = []
        i = j = 0

        while i < len(left) and j < len(right):
            left_val = left[i][column_index]
            right_val = right[j][column_index]

            # check if both values are integers
            if left_val.isdigit() and right_val.isdigit():
                left_val = int(left_val)
                right_val = int(right_val)

                if left_val < right_val:
                    result.append(left[i])
                    i += 1
                else:
                    result.append(right[j])
                    j += 1
            else:


                # if both are integers then we compare as a string
                if left_val < right_val:
                    result.append(left[i])
                    i += 1
                else:
                    result.append(right[j])
                    j += 1

        result.extend(left[i:])
        result.extend(right[j:])
        return result

    def merge_sort(self,arr, column_index):
        if len(arr) <= 1:
            return arr

        # divides the array into 2 parts
        mid = len(arr) // 2
        left_half = arr[:mid]
        right_half = arr[mid:]

        # recursive function call to sort the array
        left_half =self.merge_sort(left_half, column_index)
        right_half = self.merge_sort(right_half, column_index)

        # merge the sorted halves
        return self.merge(left_half, right_half, column_index)
    def bubble_sort_code(self, column_index):
        # start_time = time.time()
        # sorted_array = MergeSort(v, 0, 999)
        # end_time = time.time()
        # run_time = end_time - start_time
        data = self.get_data_from_table()
        n = len(data)

        start_time = time.time()
        for i in range(n):
            swapped = False
            for j in range(0, n - i - 1):
                # convert the data to integers if possible
                try:
                    value1 = int(data[j][column_index])
                    value2 = int(data[j + 1][column_index])
                    if value1 > value2:
                        data[j], data[j + 1] = data[j + 1], data[j]
                        swapped = True
                except ValueError:
                    # if try fails, compare them as strings
                    if data[j][column_index] > data[j + 1][column_index]:
                        data[j], data[j + 1] = data[j + 1], data[j]
                        swapped = True
            if not swapped:
                break
        end_time = time.time()
        run_time = end_time - start_time


        self.time_label.setText(f"sorting Time: {run_time:.2f} sec")
        self.update_table(data, column_index)


    def insertion_sort_code(self,column_index):
        data = self.get_data_from_table()
        n = len(data)

        start_time = time.time()
        for i in range(1, len(data)):
            key = data[i]
            j = i - 1

            while j >= 0:
                left_value = data[j][column_index]
                right_value = key[column_index]

                # Check if both values are integers
                if left_value.isdigit() and right_value.isdigit():
                    left_value = int(left_value)
                    right_value = int(right_value)

                    if left_value > right_value:
                        data[j + 1] = data[j]
                        j -= 1
                    else:
                        break
                else:
                    # if they are not integers then compare  as  a string
                    if left_value > right_value:
                        data[j + 1] = data[j]
                        j -= 1
                    else:
                        break

            data[j + 1] = key
        end_time = time.time()
        run_time = end_time - start_time

        self.time_label.setText(f"Sorting Time: {run_time:.2f} seconds")

        self.update_table(data, column_index)

    def selection_sort_code(self,column_index):
        data=self.get_data_from_table()

        start_time = time.time()
        for i in range(len(data)):
            min_idx = i

            for j in range(i + 1, len(data)):
                left_value = data[min_idx][column_index]
                right_value = data[j][column_index]

                # check if they are  integers
                if left_value.isdigit() and right_value.isdigit():
                    left_value = int(left_value)
                    right_value = int(right_value)

                    if left_value > right_value:
                        min_idx = j
                else:
                    # if they are not both integers then  compare  as a strings
                    if left_value > right_value:
                        min_idx = j

            data[i], data[min_idx] = data[min_idx], data[i]
        end_time = time.time()
        run_time = end_time - start_time

        self.time_label.setText(f"Sorting Time: {run_time:.2f} seconds")
        self.update_table(data, column_index)

    def merge_sort_code(self,column_index):
       result = []
       data = self.get_data_from_table()


       start_time = time.time()
       result = self.merge_sort(data,column_index)
       end_time = time.time()
       run_time = end_time - start_time

       self.time_label.setText(f"Sorting Time: {run_time:.2f} seconds")
       self.update_table(result, column_index)




#asdfasafs
    def get_data_from_table(self):
        rows = self.tableWidget.rowCount()
        cols = self.tableWidget.columnCount()
        data = []

        for row in range(rows):
            row_data = []
            for col in range(cols):
                item = self.tableWidget.item(row, col)
                row_data.append(item.text())
            data.append(row_data)

        return data

    def linear_search(self, search_element, column_index):
        data = self.get_data_from_table()
        found_rows = []

        for row_index, row_data in enumerate(data):
            if column_index < len(row_data) and row_data[column_index] == search_element:
                found_rows.append(row_index)
        temp = data[1]
        data[1] = data[found_rows[0]]
        data[found_rows[0]] = temp
        self.update_table(data,  column_index)


    def load_data_from_csv(self):
        file_path = r'A:\scrapping data\data.csv'
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                data = list(csv.reader(file))

            self.tableWidget.setRowCount(len(data))
            for row_index, row_data in enumerate(data):
                for col_index, cell_data in enumerate(row_data):
                    item = QtWidgets.QTableWidgetItem(cell_data)
                    self.tableWidget.setItem(row_index, col_index, item)
        except FileNotFoundError:
            print(f"File not found: {file_path}")

    def update_table(self, data, column_index):
        for row, row_data in enumerate(data):
            for col, cell_data in enumerate(row_data):
                item = QtWidgets.QTableWidgetItem(cell_data)
                self.tableWidget.setItem(row, col, item)

def main():
      app = QtWidgets.QApplication(sys.argv)
      window = MainWindow()
      window.show()
      sys.exit(app.exec_())

if __name__ == "__main__":
     main()